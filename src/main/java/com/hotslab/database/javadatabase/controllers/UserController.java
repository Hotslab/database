package com.hotslab.database.javadatabase.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hotslab.database.javadatabase.HibernateUtils;
import com.hotslab.database.javadatabase.exception.ResourceNotFoundException;
import com.hotslab.database.javadatabase.models.User;
import com.hotslab.database.javadatabase.repository.UserRepository;

@RestController
@RequestMapping("/api/v1")
public class UserController {
    @Autowired
    private CacheManager cacheManager;
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/users")
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<User> getEmployeeById(@PathVariable(value = "id") Long userId)
            throws ResourceNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + userId));
        return ResponseEntity.ok().body(user);
    }

    @PostMapping("/users")
    public User createEmployee(@Valid @RequestBody User user) {
        return userRepository.save(user);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<User> updateEmployee(@PathVariable(value = "id") Long userId,
            @Valid @RequestBody User userDetails) throws ResourceNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + userId));

        user.setName(userDetails.getName());
        user.setSurname(userDetails.getSurname());
        user.setPhoneNumber(userDetails.getPhoneNumber());
        user.setEmail(userDetails.getEmail());
        user.setIdType(userDetails.getIdType());
        user.setIdNumber(userDetails.getIdNumber());
        user.setPassword(userDetails.getPassword());
        user.setStreet(userDetails.getStreet());
        user.setSurburb(userDetails.getSurburb());
        user.setCity(userDetails.getCity());
        user.setCountry(userDetails.getCountry());
        user.setCurrency(userDetails.getCurrency());
        user.setPostCode(userDetails.getPostCode());
        user.setBankCardNumber(userDetails.getBankCardNumber());
        user.setBankAccountNumber(userDetails.getBankAccountNumber());
        user.setBankName(userDetails.getBankName());
        user.setBankSwiftCode(userDetails.getBankSwiftCode());

        final User updatedUser = userRepository.save(user);
        return ResponseEntity.ok(updatedUser);
    }

    @DeleteMapping("/users/{id}")
    public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Long userId)
            throws ResourceNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + userId));

        userRepository.delete(user);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/clear")
    public List<String> clearAllCaches() {
        int count = 0;
        List<String> clearedCache = new ArrayList<String>();
        Session session = HibernateUtils.getSessionFactory().openSession();
        session.clear();
        for (String name : cacheManager.getCacheNames()) {
            clearedCache.add(count++ + ". Clearing " + name);
            cacheManager.getCache(name).clear();
        }
        return clearedCache;
    }
}
