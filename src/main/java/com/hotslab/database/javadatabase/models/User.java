package com.hotslab.database.javadatabase.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Table(name = "users")
public class User {

    private long id;
    private String name;
    private String surname;
    private String phoneNumber;
    private String email;
    private String idType;
    private String idNumber;
    private String password;
    private String createdAt;
    private String updatedAt;
    private String street;
    private String surburb;
    private String city;
    private String country;
    private String currency;
    private String postCode;
    private String bankCardNumber;
    private String bankAccountNumber;
    private String bankName;
    private String bankSwiftCode;

    public User() {
    }

    public User(String name, String surname, String phoneNumber, String email, String idType, String idNumber,
            String password, String street, String surburb, String city, String country, String currency,
            String postCode, String bankCardNumber, String bankAccountNumber, String bankName, String bankSwiftCode) {
        this.name = name;
        this.surname = surname;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.idType = idType;
        this.idNumber = idNumber;
        this.password = password;
        this.createdAt = this.timeString();
        this.updatedAt = this.timeString();
        this.street = street;
        this.surburb = surburb;
        this.city = city;
        this.country = country;
        this.currency = currency;
        this.postCode = postCode;
        this.bankCardNumber = bankCardNumber;
        this.bankAccountNumber = bankAccountNumber;
        this.bankName = bankName;
        this.bankSwiftCode = bankSwiftCode;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "surname", nullable = false)
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Column(name = "phone_number", nullable = false)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Column(name = "email", nullable = false)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "id_type", nullable = false)
    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    @Column(name = "id_number", nullable = false)
    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    @Column(name = "password", nullable = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "created_at", nullable = false)
    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Column(name = "updated_at", nullable = false)
    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Column(name = "street", nullable = false)
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Column(name = "surburb", nullable = false)
    public String getSurburb() {
        return surburb;
    }

    public void setSurburb(String surburb) {
        this.surburb = surburb;
    }

    @Column(name = "city", nullable = false)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column(name = "country", nullable = false)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Column(name = "currency", nullable = false)
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Column(name = "postCode", nullable = false)
    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @Column(name = "bank_card_number", nullable = false)
    public String getBankCardNumber() {
        return bankCardNumber;
    }

    public void setBankCardNumber(String bankCardNumber) {
        this.bankCardNumber = bankCardNumber;
    }

    @Column(name = "bank_account_number", nullable = false)
    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    @Column(name = "bank_name", nullable = false)
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Column(name = "bank_swift_code", nullable = false)
    public String getBankSwiftCode() {
        return bankSwiftCode;
    }

    public void setBankSwiftCode(String bankSwiftCode) {
        this.bankSwiftCode = bankSwiftCode;
    }

    public String timeString() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formatDateTime = now.format(formatter);
        return formatDateTime;
    }

    @Override
    public String toString() {
        return "Employee [" + "id: " + id + ", " + "name: " + name + ", " + "surname: " + surname + ", "
                + "phoneNumber: " + phoneNumber + ", " + "email: " + email + ", " + "idType: " + idType + ", "
                + "idNumber: " + idNumber + ", " + "password: " + password + ", " + "createdAt: " + createdAt + ", "
                + "updatedAt: " + updatedAt + ", " + "street: " + street + ", " + "surburb: " + surburb + ", "
                + "city: " + city + ", " + "country: " + country + ", " + "currency: " + currency + ", " + "postCode: "
                + postCode + ", " + "bankCardNumber: " + bankCardNumber + ", " + "bankAccountNumber: "
                + bankAccountNumber + ", " + "bankName: " + bankName + ", " + "bankSwiftCode: " + bankSwiftCode + "]";
    }
}
