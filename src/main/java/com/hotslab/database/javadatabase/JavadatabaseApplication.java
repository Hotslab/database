package com.hotslab.database.javadatabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan({ "com.hotslab.database.javadatabase.exception", "com.hotslab.database.javadatabase.models",
		"com.hotslab.database.javadatabase.repository", "com.hotslab.database.javadatabase.controllers" })
@EnableJpaRepositories("com.hotslab.database.javadatabase.repository")
@EnableCaching
@SpringBootApplication
public class JavadatabaseApplication {
	public static void main(String[] args) {
		SpringApplication.run(JavadatabaseApplication.class, args);
	}
}
